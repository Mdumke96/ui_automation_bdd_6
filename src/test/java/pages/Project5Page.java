package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.Driver;

public class Project5Page {

    public Project5Page(){
        PageFactory.initElements(Driver.getDriver(), this);
    }

    @FindBy(css = "h1[class*='is-size-2']")
    public WebElement paginationHeading;

    @FindBy(id = "sub_heading")
    public WebElement worldCityPopulationHeading;

    @FindBy(id = "content")
    public WebElement paragraphText;

    @FindBy(id = "previous")
    public WebElement previousButton;

    @FindBy(id = "next")
    public WebElement nextButton;

    @FindBy(css = "p[class*='city_info']")
    public WebElement cityInfo;

    @FindBy(css = "p[class*='country_info']")
    public WebElement countryInfo;

    @FindBy(css = "p[class*='population_info']")
    public WebElement populationInfo;
}
