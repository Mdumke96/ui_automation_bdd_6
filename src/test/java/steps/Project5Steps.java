package steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import pages.Project4Page;
import pages.Project5Page;
import utils.Driver;
import utils.Waiter;

import java.util.ArrayList;
import java.util.List;

public class Project5Steps {

    WebDriver driver;
    Project5Page project5Page;

    @Before
    public void setDriver() {
        driver = Driver.getDriver();
        project5Page = new Project5Page();
    }

    @Given("the user navigates to  {string}")
    public void theUserNavigatesTo(String url) {
        driver.get(url);
    }

    @Then("the user should see the “Pagination” heading")
    public void theUserShouldSeeThePaginationHeading() {
        Assert.assertTrue(project5Page.paginationHeading.isDisplayed());
    }

    @And("the user should see the “World City Populations” heading")
    public void theUserShouldSeeTheWorldCityPopulationsHeading() {
        Assert.assertTrue(project5Page.worldCityPopulationHeading.isDisplayed());
    }

    @And("the user should see the “What are the most populated cities in the world? Here is a list of the top five most populated cities in the world:” paragraph")
    public void theUserShouldSeeTheWhatAreTheMostPopulatedCitiesInTheWorldHereIsAListOfTheTopFiveMostPopulatedCitiesInTheWorldParagraph() {
        Assert.assertTrue(project5Page.paragraphText.isDisplayed());
    }

    @Then("the user should see the “Previous” button is disabled")
    public void theUserShouldSeeThePreviousButtonIsDisabled() {
        Assert.assertFalse(project5Page.previousButton.isEnabled());
    }

    @And("the user should see the “Next” button is enabled")
    public void theUserShouldSeeTheNextButtonIsEnabled() {
        Assert.assertTrue(project5Page.nextButton.isEnabled());
    }

    @When("the user clicks on the “Next” button")
    public void theUserClicksOnTheNextButton() {
        project5Page.nextButton.click();
    }

    @Then("the user should see the “Previous” button is enabled")
    public void theUserShouldSeeThePreviousButtonIsEnabled() {
        Assert.assertTrue(project5Page.previousButton.isEnabled());
    }

    @When("the user clicks on the “Next” button till it becomes disabled")
    public void theUserClicksOnTheNextButtonTillItBecomesDisabled() {
        project5Page.nextButton.click();
        project5Page.nextButton.click();
        project5Page.nextButton.click();
    }

    @And("the user should see the “Next” button is disabled")
    public void theUserShouldSeeTheNextButtonIsDisabled() {
        Assert.assertFalse(project5Page.nextButton.isEnabled());
    }

    @Then("the user should see “Tokyo” City with the info below and an image")
    public void theUserShouldSeeTokyoCityWithTheInfoBelowAndAnImage(DataTable dataTable) {
        List<String> expectedText = dataTable.asList();
        List<String> list = new ArrayList<>();
        list.add(project5Page.cityInfo.getText());
        list.add(project5Page.countryInfo.getText());
        list.add(project5Page.populationInfo.getText());

        for(int i = 0; i < expectedText.size(); i++){
            Assert.assertEquals(expectedText.get(i), list.get(i));
        }
    }

    @Then("the user should see “Delhi” City with the info below and an image")
    public void theUserShouldSeeDelhiCityWithTheInfoBelowAndAnImage(DataTable dataTable) {
        Waiter.pause(2);

        List<String> expectedText = dataTable.asList();
        List<String> list = new ArrayList<>();
        list.add(project5Page.cityInfo.getText());
        list.add(project5Page.countryInfo.getText());
        list.add(project5Page.populationInfo.getText());

        for(int i = 0; i < expectedText.size(); i++){
            Assert.assertEquals(expectedText.get(i), list.get(i));
        }
    }

    @Then("the user should see “Shangai“ City with the info below and an image")
    public void theUserShouldSeeShangaiCityWithTheInfoBelowAndAnImage(DataTable dataTable) {
        Waiter.pause(2);

        List<String> expectedText = dataTable.asList();
        List<String> list = new ArrayList<>();
        list.add(project5Page.cityInfo.getText());
        list.add(project5Page.countryInfo.getText());
        list.add(project5Page.populationInfo.getText());

        for(int i = 0; i < expectedText.size(); i++){
            Assert.assertEquals(expectedText.get(i), list.get(i));
        }

        //Bug being Shanghai is incorrectly spelled
    }

    @Then("the user should see “Sao Paulo“ City with the info below and an image")
    public void theUserShouldSeeSaoPauloCityWithTheInfoBelowAndAnImage(DataTable dataTable) {
        Waiter.pause(2);

        List<String> expectedText = dataTable.asList();
        List<String> list = new ArrayList<>();
        list.add(project5Page.cityInfo.getText());
        list.add(project5Page.countryInfo.getText());
        list.add(project5Page.populationInfo.getText());

        for(int i = 0; i < expectedText.size(); i++){
            Assert.assertEquals(expectedText.get(i), list.get(i));
        }

        //Bug being Brazil is incorrectly spelled
    }

    @Then("the user should see “Mexico City” City with the info below and an image")
    public void theUserShouldSeeMexicoCityCityWithTheInfoBelowAndAnImage(DataTable dataTable) {
        Waiter.pause(2);

        List<String> expectedText = dataTable.asList();
        List<String> list = new ArrayList<>();
        list.add(project5Page.cityInfo.getText());
        list.add(project5Page.countryInfo.getText());
        list.add(project5Page.populationInfo.getText());

        for(int i = 0; i < expectedText.size(); i++){
            Assert.assertEquals(expectedText.get(i), list.get(i));
        }
    }
}
