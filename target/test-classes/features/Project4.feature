Feature: Project 4 requirements

  Scenario: Validate the default content
    Given the user navigates to "https://techglobal-training.com/frontend/project-4"
    Then the user should see the “Inventory” heading
    And the user should see the table with the headers below
    |Quantity |
    |Product  |
    |Price $  |
    |Total $  |
    And the user should see the table with the rows below
    And the user should see the “ADD PRODUCT” button is enabled
    And the user should see the “Total = $” text displayed

  Scenario: Validate the new product modal
      Given the user navigates to "https://techglobal-training.com/frontend/project-4"
      When the user clicks on the “ADD PRODUCT” button
      Then the user should see the “Add New Product” modal with its heading
      And the user should see the “X” button is enabled
      And the user should see the “Please select the quantity” label
      And the user should see the “Quantity” input box is enabled
      And the user should see the “Please enter the name of the product” label
      And the user should see the “Product” input box is enabled
      And the user should see the “Please enter the price of the product” label
      And the user should see the “Price” input box is enabled
      And the user should see the “SUBMIT” button is enabled

  Scenario: Validate the add new product modal button
    Given the user navigates to "https://techglobal-training.com/frontend/project-4"
    When the user clicks on the “ADD PRODUCT” button
    Then the user should see the “Add New Product” modal with its heading
    When the user clicks on the “X” button
    Then the user should not see the “Add New Product” modal

  Scenario: Validate the new product added
    Given the user navigates to "https://techglobal-training.com/frontend/project-4"
    When the user clicks on the “ADD PRODUCT” button
    And the user enters the quantity as “”
    And the user enters the product as “”
    And the user enters the price as “”
    And the user clicks on the “SUBMIT” button
    Then the user should see the table with the new row below
    And the user should see the “Total = $2500” text displayed